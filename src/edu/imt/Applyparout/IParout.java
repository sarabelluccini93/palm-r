package edu.imt.Applyparout;

import edu.imt.specification.operators.Process;

public interface IParout {

	Process interpreter(Process p);
}
